import numpy as np
import pandas as pd
import holidays

from fashion_sales_forecast import config


def parse_initial_dataframe(
    df: pd.DataFrame, columns: list[str] = None
) -> pd.DataFrame:
    """Parses the dtypes of the raw dataframe to efficient and usable dtypes

    Args:
        df (pd.DataFrame): Raw dataframe including the fashion sales data set
        columns (list[str], optional): Column specifier, necessary for
            DataframeTransformer. Defaults to None.

    Returns:
        pd.DataFrame: DataFrame with suitable dtypes
    """
    date_columns = [col for col in df.columns if "date" in col]
    bool_columns = ["is_promotion"]
    category_columns = [
        "article_id",
        "style_color_group",
        "style_group",
        "level1",
        "level2",
        "level3",
        "level4",
        "sales_channel",
        "current_season",
        "initial_season",
    ]

    assert all(col in df.columns for col in date_columns), "Missing columns in df"
    assert all(col in df.columns for col in bool_columns), "Missing columns in df"
    assert all(col in df.columns for col in category_columns), "Missing columns in df"

    for col in date_columns:
        df = df.assign(**{col: lambda df: pd.to_datetime(df[col])})
    for col in bool_columns:
        df = df.assign(**{col: lambda df: df[col].astype(bool)})
    for col in category_columns:
        df = df.assign(**{col: lambda df: df[col].astype("category")})

    return df


def filter_by_price(
    df: pd.DataFrame, price: float, price_column: str = "original_price"
) -> pd.DataFrame:
    """Filter dataframe by price column, only keeping a specific price.

    Args:
        df (pd.DataFrame): DataFrame containing ``price_column``
        price (float): Price value to be filtered by
        price_column (str, optional): Column containing the price information.
            Defaults to "original_price".

    Returns:
        pd.DataFrame: _description_
    """
    return df.loc[np.abs(df[price_column] - price) < 1e-14]


def get_holidays_df(years: list[int]) -> pd.DataFrame:
    """Provides a DataFrame with all German holidays in the provided years

    Args:
        years (list[int]): List with years to be included.

    Returns:
        pd.DataFrame: DataFrame containing holidays and dates of the specified years
    """
    holidays_DE = holidays.Germany(years=years)
    df_holidays = pd.DataFrame(
        {
            "holiday": holidays_DE.values(),
            "date": holidays_DE.keys(),
        }
    )
    return df_holidays


def compute_rolling_mean(
    df: pd.DataFrame, columns: list[str], days: int = 7
) -> pd.DataFrame:
    """Computes rolling mean and standard deviation with respect to the date

    Args:
        df (pd.DataFrame): DataFrame containing date-dependent sales infos
        columns (list[str]): Columns to be included in the rolling calculations
        days (int, optional): Size of the time window for the rolling aggregations.
            Defaults to 7.

    Returns:
        pd.DataFrame: _description_
    """
    df_rolling = (
        df.groupby([config.DATE_COL], as_index=False)
        .agg(
            **{
                **{f"{col}_mean": (col, "mean") for col in columns},
                **{f"{col}_std": (col, "std") for col in columns},
            }
        )
        .set_index(config.DATE_COL)
        .rolling(window=days)
        .mean()
        .reset_index()
    )
    return df_rolling
