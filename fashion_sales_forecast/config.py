from git_root import git_root

DATA_PATH: str = f"{git_root()}/data"
SALES_DATA_PATH: str = f"{DATA_PATH}/transaction_grid.csv"
MODEL_ARTIFACT_PATH: str = f"{git_root()}/model_artifacts"
DATE_COL: str = "transaction_date"

NUM_LAGS: int = 14
FEATURE_PIPELINE_FILE: str = "feature_preparation_pipeline.pkl"
SELECTED_FEATURES_PATH = f"{MODEL_ARTIFACT_PATH}/selected_features.yaml"
LABEL_COLUMN: str = "units_sold"
MODEL_FILENAME: str = "model.joblib"
