import joblib
from lightgbm.sklearn import LGBMRegressor

from fashion_sales_forecast import config


def save_model(model: LGBMRegressor):
    filename = f"{config.MODEL_ARTIFACT_PATH}/{config.MODEL_FILENAME}"
    joblib.dump(model, filename)


def load_model():
    filename = f"{config.MODEL_ARTIFACT_PATH}/{config.MODEL_FILENAME}"
    loaded_model = joblib.load(filename)
    return loaded_model
