import pandas as pd
import numpy as np
from scipy.stats import chi2_contingency
from sklearn.model_selection import TimeSeriesSplit
from sklearn.metrics import mean_absolute_error
from fashion_sales_forecast import config
from typing import Generator
from lightgbm.sklearn import LGBMRegressor

from fashion_sales_forecast.features import create_lagged_feature


def compute_cramers_v(x: pd.Series, y: pd.Series) -> float:
    """Computes so-called Cramer's V between two categorical features
    (see: https://en.wikipedia.org/wiki/Cram%C3%A9r%27s_V)
    It can be interpreted similar to a correlation of continuous variables.

    Args:
        x (pd.Series): Values of the column of one categorical variable
        y (pd.Series): Values of the column of the other categorical variable

    Returns:
        float: Cramer'v -- a measure of how dependent two categorical variables are
    """
    confusion_matrix = pd.crosstab(x, y)
    chi2, p, dof, expected = chi2_contingency(confusion_matrix)
    num_samples = confusion_matrix.sum().sum()
    num_rows = confusion_matrix.shape[0]
    num_cols = confusion_matrix.shape[1]
    cramers_v = confusion_matrix.shape[1]
    cramers_v = np.sqrt(chi2 / (num_samples * min(num_rows - 1, num_cols - 1)))
    return cramers_v


def categorical_correlation_matrix(
    df: pd.DataFrame, categorical_features: list[str]
) -> pd.DataFrame:
    """Computes a correlation matrix for categorical variables based on chi^2 tests

    Args:
        df (pd.DataFrame): DataFrame containing categorical columns
        categorical_features (list[str]): All categorical columns to consider.

    Returns:
        pd.DataFrame: Matrix containing categorical correlations.
    """
    df_categorical = df.filter(items=categorical_features)

    matrix = pd.DataFrame(index=categorical_features, columns=categorical_features)
    for feature_1 in categorical_features:
        for feature_2 in categorical_features:
            corr = compute_cramers_v(
                df_categorical[feature_1], df_categorical[feature_2]
            )
            matrix[feature_1][feature_2] = corr
    return matrix


def get_cross_validation_splits_generator(
    df: pd.DataFrame, n_splits: int = 3
) -> Generator:
    """Creates a custom generator for cross-validation splits for time series
    predictions. It's based on sklearn's TimeSeriesSplit and keeps the validation and
    training set separated in time.

    Args:
        df (pd.DataFrame): DataFrame containing date-related features.

    Yields:
        Generator: Generator providing train/validation splits.
    """
    dates = df[config.DATE_COL].sort_values().drop_duplicates().values

    tscv = TimeSeriesSplit(n_splits=n_splits)
    train_valid_splits = tscv.split(dates)

    for cv in train_valid_splits:
        dates_train_cv = dates[cv[0]]
        dates_valid_cv = dates[cv[1]]
        df_train_cv = df.loc[df[config.DATE_COL].isin(dates_train_cv)].index
        df_valid_cv = df.loc[df[config.DATE_COL].isin(dates_valid_cv)].index
        yield df_train_cv, df_valid_cv


def mean_absolute_scaled_error(actual, predicted):
    """
    Calculate MASE (Mean Absolute Scaled Error). This can be interpreted as the MAE
    compared to the naive model of predicting the previous value of the time series
    """
    mae = mean_absolute_error(actual, predicted)
    naive_error = np.mean(np.abs(actual[1:] - actual[:-1]))
    return mae / naive_error


def create_future_labels(df: pd.DataFrame, days_in_future: int = 14) -> pd.DataFrame:
    """Creates future values of the label columns for evaluation for several consecutive
    model predictions.

    Args:
        df (pd.DataFrame): Dataframe containing date, article_id and label column.
        days_in_future (int, optional): Days up to which prediction should be made.
            (=forecast horizon). Defaults to 14.

    Returns:
        pd.DataFrame: DataFrame including columns for future values of the label
    """
    df = df.pipe(create_lagged_feature, n_days_back=range(-1, -days_in_future - 1, -1))
    return df


def predict_several_days(
    df: pd.DataFrame,
    model: LGBMRegressor,
    selected_features: list[str],
    n_days: int = 14,
) -> pd.DataFrame:
    """Computing consecutive predictions after each other. While we expect the ordinary
    features (incl. prices, discounts, etc.) to be known at prediction time, we need to
    update the lagged features.

    Args:
        df (pd.DataFrame): DataFrame containing all features as specified.
        model (LGBMRegressor): Trained model for prediction.
        selected_features (list[str]): Features used during training.
        n_days (int, optional): Forecast horizon. Defaults to 14.

    Returns:
        pd.DataFrame: Result DataFrame containing future predictions of several days.
    """
    article_dfs = []
    for article_id in df["article_id"].unique():
        df_article = df.query(f"article_id == '{article_id}'").sort_values(
            by=config.DATE_COL
        )
        for n_pred in range(1, n_days + 1):
            # compute predictions
            prediction_col = f"predict_{n_pred}"
            df_article[prediction_col] = model.predict(df_article[selected_features])

            # update lag columns that contain existing units sold data
            for lag in range(n_pred, 14 + 1):
                col_name = f"units_sold_lag_{lag}"
                if col_name in df_article.columns:
                    df_article[f"units_sold_lag_{lag + 1}"] = df_article[
                        col_name
                    ].shift(1)

            # update lag columns from predictions where there is no existing units sold data
            for lag in range(1, n_pred):
                col_name = f"units_sold_lag_{lag}"
                if col_name in df_article.columns:
                    df_article[f"units_sold_lag_{lag + 1}"] = df_article[
                        col_name
                    ].shift(1)

            # update first lag by prediction
            df_article["units_sold_lag_1"] = df_article[f"predict_{n_pred}"].shift(1)
        article_dfs.append(df_article.dropna())
    return pd.concat(article_dfs)


def compute_time_dependent_metric(df_result: pd.DataFrame, n_days: int = 14):
    """
    Computes metrics depending on how many days in the future the forecast is.
    """
    prediction_range = range(1, n_days + 1)
    metric = []
    for i in prediction_range:
        metric.append(
            mean_absolute_error(
                df_result[f"units_sold_future_{i}"], df_result[f"predict_{i}"]
            )
        )

    df_metric = pd.DataFrame(
        {
            "MAE": metric,
            "prediction_day": prediction_range,
        }
    )
    return df_metric
