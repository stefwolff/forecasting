import yaml  # type: ignore
import pandas as pd
import numpy as np
from typing import Union
from datetime import datetime, timedelta
import calendar
from pandas._libs.tslibs.timestamps import Timestamp
import joblib
from sklearn.pipeline import Pipeline

from fashion_sales_forecast import config


class DataframeTransformer:
    """Custom sklearn transformer that can operate on pandas DataFrames
    This does not fit anything but only applies a passed function to a
    dataframe. Columns can be passed and used in the function
    """

    def __init__(self, func, columns: list[str] = None, **kwargs):
        self.func = func
        self.columns = columns if columns else []
        for kw in kwargs:
            self.kw = kwargs[kw]

    def transform(self, df, **transform_params):
        return self.func(df, self.columns)

    def fit(self, X, y=None, **fit_params):
        return self


def create_fraction_of_year(df: pd.DataFrame, colums: list[str] = None) -> pd.DataFrame:
    """Creates a float value variable representing how much of the year has passed. It
    is further transformed by a sine function in order to represent how far summer and
    winter are away from the current time point.

    Args:
        df (pd.DataFrame): DataFrame containing ``config.DATE_COL``
        colums (list[str], optional): Default argument to be passed to the
            DataframeTransformer. Defaults to None.

    Returns:
        pd.DataFrame: DataFrame containing new "fraction_of_year" feature.
    """

    def _sin_encoding_of_day_of_year(date: Timestamp) -> float:
        if not isinstance(date, Timestamp):
            raise ValueError("The date must be a pandas ``Timestamp`` object")

        day_of_year = date.day_of_year
        total_days_in_year = 366 if calendar.isleap(date.year) else 365
        fraction_of_year = day_of_year / total_days_in_year
        return np.cos(2 * np.pi * fraction_of_year)

    df = df.assign(
        fraction_of_year=lambda df: df[config.DATE_COL].apply(
            _sin_encoding_of_day_of_year
        )
    )
    return df


def create_day_of_week(df: pd.DataFrame, columns: list[str]) -> pd.DataFrame:
    """
    Creates the feature corresponding to the day of the week of ``config.DATE_COL``
    """
    df = df.assign(
        transaction_day_of_week=lambda df: df[config.DATE_COL].apply(
            lambda x: x.weekday()
        )
    )
    return df


def create_feature_month_columns(df: pd.DataFrame, columns: list[str]) -> pd.DataFrame:
    """
    Creates the feature corresponding to the month of ``config.DATE_COL``
    """
    df = df.assign(
        transaction_month=lambda df: df[config.DATE_COL].apply(lambda x: x.month)
    )
    return df


def create_feature_is_initial_season(
    df: pd.DataFrame, columns: list[str]
) -> pd.DataFrame:
    """
    Adds a bool feature the determines if the current season is the initial season
    of the product.
    """
    df = df.assign(
        is_initial_season=lambda df: df.apply(
            lambda row: row["current_season"] == row["initial_season"],
            axis=1,
        )
    )
    return df


def create_feature_days_to_season_dates(
    df: pd.DataFrame, columns: list[str]
) -> pd.DataFrame:
    """
    Adds features that contain the number of days to the start/end of the current
    season
    """

    def _compute_days_to_season_end(row):
        days_to_season_end = row["season_end_date"] - row[config.DATE_COL]
        return days_to_season_end.days

    def _compute_days_since_season_start(row):
        days_since_season_start = row[config.DATE_COL] - row["season_start_date"]
        return days_since_season_start.days

    df = df.assign(
        days_to_season_end=lambda df: df.apply(
            lambda row: _compute_days_to_season_end(row), axis=1
        )
    ).assign(
        days_since_season_start=lambda df: df.apply(
            lambda row: _compute_days_since_season_start(row), axis=1
        )
    )
    return df


def create_feature_time_of_year(df: pd.DataFrame, columns: list[str]) -> pd.DataFrame:
    """
    Adds a time of the year feature for the transaction date.
    """
    season_map = {
        **{month: "winter" for month in [12, 1, 2]},
        **{month: "spring" for month in [3, 4, 5]},
        **{month: "summer" for month in [6, 7, 8]},
        **{month: "fall" for month in [9, 10, 11]},
    }

    df = df.assign(
        time_of_year=lambda df: df[config.DATE_COL].apply(lambda x: season_map[x.month])
    ).assign(
        time_of_year_launch=lambda df: df["month_of_launch"].apply(
            lambda x: season_map[x]
        )
    )

    df["time_of_year"] = df["time_of_year"].astype("category")
    df["time_of_year_launch"] = df["time_of_year_launch"].astype("category")

    return df


def create_feature_is_in_black_week(
    df: pd.DataFrame,
    columns: list[str] = None,
) -> pd.DataFrame:
    """
    Adds feature that checks if the current date is in the black week at the end
    of November
    """

    def _is_in_black_week(date: Timestamp) -> bool:
        year = date.year
        thanksgiving = datetime(year, 11, 1)
        while thanksgiving.weekday() != 3:
            thanksgiving += timedelta(days=1)
        thanksgiving += timedelta(weeks=3)

        black_friday = thanksgiving + timedelta(days=1)
        return black_friday <= date < black_friday + timedelta(days=7)

    df = df.assign(
        in_black_week=lambda df: df[config.DATE_COL].apply(_is_in_black_week)
    )
    return df


def create_feature_week_after_xmas(
    df: pd.DataFrame,
    columns: list[str] = None,
) -> pd.DataFrame:
    """
    Adds bool feature for the current date being in the week after Christmas
    """

    def _christmas_is_in_last_week(date: Timestamp) -> bool:
        date_one_week_ago = date - timedelta(7)
        date_christmas = datetime(date_one_week_ago.year, 12, 24)
        return date_one_week_ago < date_christmas

    df = df.assign(
        xmas_in_last_week=lambda df: df[config.DATE_COL].apply(
            _christmas_is_in_last_week
        )
    )
    return df


def create_feature_start_spring_fall_season(
    df: pd.DataFrame,
    columns: list[str] = None,
) -> pd.DataFrame:
    """
    Adds bool feature for the current date being in the start of the spring/fall season
    """

    def _is_start_spring_season(date: Timestamp) -> bool:
        return date.month in [2, 3]

    def _is_start_fall_season(date: Timestamp) -> bool:
        return date.month in [9, 10]

    df = df.assign(
        is_start_spring=lambda df: df[config.DATE_COL].apply(_is_start_spring_season)
    ).assign(is_start_fall=lambda df: df[config.DATE_COL].apply(_is_start_fall_season))
    return df


def create_feature_discounts(df: pd.DataFrame, columns: list[str]) -> pd.DataFrame:
    """
    Adds features for absolute und relative discounts of the differnt price levels.
    """
    df = (
        df.assign(
            absolut_discount_sales=lambda df: df.apply(
                lambda row: row["original_price"] - row["sales_price"], axis=1
            )
        )
        .assign(
            relative_discount_sales=lambda df: df.apply(
                lambda row: (row["original_price"] - row["sales_price"])
                / row["original_price"],
                axis=1,
            )
        )
        .assign(
            absolut_discount_final_price=lambda df: df.apply(
                lambda row: row["sales_price"] - row["final_price"],
                axis=1,
            )
        )
        .assign(
            relative_discount_final_price=lambda df: df.apply(
                lambda row: (row["sales_price"] - row["final_price"])
                / row["sales_price"],
                axis=1,
            )
        )
    )
    return df


def create_feature_discounts_prev_week(
    df: pd.DataFrame, columns: list[str]
) -> pd.DataFrame:
    """
    Create a rolling mean feature including the average discount over the previous year
    """
    if "absolut_discount_final_price" not in df:
        raise ValueError("Column 'absolut_discount_final_price' not in dataframe")

    df_rolling_mean = (
        df.sort_values(["article_id", config.DATE_COL])
        .groupby("article_id", as_index=False, observed=True)
        .rolling(window=7, on=config.DATE_COL)["absolut_discount_final_price"]
        .mean()
        .reset_index()
        .rename(columns={"absolut_discount_final_price": "discount_mean_prev_week"})
    )
    df = df.merge(df_rolling_mean, on=["article_id", config.DATE_COL], how="left")
    return df


def create_lagged_feature(
    df: pd.DataFrame,
    columns: list[str] = None,
    lag_column: str = "units_sold",
    n_days_back: Union[int, list[int]] = 1,
) -> pd.DataFrame:
    """Creats lagged features, that include the past of the time series

    Args:
        df (pd.DataFrame): Dataframe containing dates and values
        columns (list[str], optional): . Defaults to None.
        lag_column (str, optional): Column that is used for computing lags. In the
            standard case this is the label column itself. Defaults to "units_sold".
        n_days_back (Union[int, list[int]], optional): List of ints of days back that
            should be covered. Defaults to 1.

    Returns:
        pd.DataFrame: DataFrame including lagged features
    """
    if isinstance(n_days_back, int):
        n_days_back = [n_days_back]

    for n_lag in n_days_back:
        article_dfs = []
        col_lagged_feature = (
            f"{lag_column}_lag_{n_lag}"
            if n_lag > 0
            else f"{lag_column}_future_{-n_lag}"
        )

        # compute the lagged features for each article separately
        for article_id in df["article_id"].unique():
            article_dfs.append(
                df.query(f"article_id == '{article_id}'")
                .sort_values(by=config.DATE_COL)
                .assign(**{col_lagged_feature: lambda df: df[lag_column].shift(n_lag)})
                .filter(items=["article_id", config.DATE_COL, col_lagged_feature])
            )
        df_previous_units_sold = pd.concat(article_dfs)
        df = df.merge(df_previous_units_sold, on=["article_id", config.DATE_COL])

    df = (
        df.dropna()
        .astype({col: "int" for col in df.columns if col.startswith("units_sold")})
        .reset_index(drop=True)
    )
    return df


def save_feature_preparation_pipeline(pipeline: Pipeline):
    """
    Serialize pre-processing pipeline as pickle.
    """
    joblib.dump(
        pipeline,
        f"{config.MODEL_ARTIFACT_PATH}/{config.FEATURE_PIPELINE_FILE}",
    )


def load_feature_preparation_pipeline() -> Pipeline:
    """
    Load serialized pre-processing pipeline
    """
    return joblib.load(f"{config.MODEL_ARTIFACT_PATH}/{config.FEATURE_PIPELINE_FILE}")


def save_selected_features(selected_features: list[str]) -> None:
    """
    Serializes a list of names of selected features columns.
    """
    yaml_string = yaml.dump(selected_features)
    with open(config.SELECTED_FEATURES_PATH, "w") as file:
        file.write(yaml_string)


def load_selected_features() -> list[str]:
    """
    Loads a list of names of selected feature columns for a yaml file.
    """
    with open(config.SELECTED_FEATURES_PATH, "r") as file:
        selected_features = yaml.load(file, Loader=yaml.FullLoader)
    return selected_features
