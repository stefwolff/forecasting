import pandas as pd
import plotly.express as px

from fashion_sales_forecast import config
from fashion_sales_forecast.utils import get_holidays_df


def get_year_range(df: pd.DataFrame) -> list[int]:
    """Provides a year range extracted from the present dates in the data.

    Args:
        df (pd.DataFrame): Dataframe containing ``config.DATE_COL``

    Returns:
        list[int]: List with covered years.
    """
    min_year = int(df[config.DATE_COL].min().year)
    max_year = int(df[config.DATE_COL].max().year)
    return list(range(min_year, max_year + 1))


def plot_time_series_with_holidays(
    df: pd.DataFrame,
    value_col: str,
    date_col: str = config.DATE_COL,
    title: str = "evolution",
    color_col: str = None,
):
    """Helper function to plot the time series of a quantity including markers for
    public holidays.

    Args:
        df (pd.DataFrame): DataFrame containing date-dependent data.
        value_col (str): Column containing the data to be plotted
        date_col (str, optional): Column containing the date.
            Defaults to ``config.DATE_COL``.
        title (str, optional): Title of the plot. Defaults to "evolution".
        color_col (str, optional): Color of the plot. Defaults to None.
    """
    year_range = get_year_range(df)
    df_holidays = get_holidays_df(year_range)
    df_holidays["y_location"] = 0

    # plot time series
    fig = px.line(
        df,
        x=date_col,
        y=value_col,
        title=title,
        color=color_col,
    )

    # plot vertical lines at public holiday positions
    for date in df_holidays["date"]:
        fig.add_vline(x=date, line_color="black", line_dash="dot", opacity=0.5)

    # plot yellow stars at public holiday positions
    fig.add_trace(
        px.scatter(df_holidays, x="date", y="y_location", hover_data="holiday")
        .update_traces(
            marker_symbol="star",
            marker_size=10,
            marker_color="yellow",
            marker_line_width=1,
        )
        .data[0]
    )
    fig.update_layout(height=600)
    fig.update_xaxes(range=[df[date_col].min(), df[date_col].max()])
    fig.show()


def plot_price_histogram(
    df: pd.DataFrame,
    title: str,
) -> None:
    """Plot overlayed histograms for different original prices

    Args:
        df (pd.DataFrame): DataFrame containing the relevant price columns.
        title (str): Title of the plot.
    """
    df_prices = df.filter(
        items=[
            "original_price",
            "sales_price",
            "final_price",
        ]
    )

    fig = px.histogram(
        df_prices,
        nbins=60,
        range_x=[0, 60],
        barmode="overlay",
        log_y=True,
        title=title,
    )
    fig.show()
