#!/bin/bash

for notebook in "$@/notebooks"; do
  jupyter nbconvert --to html "$notebook"
#   git add "${notebook%.ipynb}.html"
done
