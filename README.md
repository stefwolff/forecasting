# Forecast fashion product sales project

## Content
The project is separated in different steps represented by the notebooks in the `notebooks` folder. Associated code is placed in the `fashion_sales_forecast` package if helpful. For simplicity, notebooks incl. output have also been added as html exports.

Roughly, the following steps are covered:
* `01_EDA_fashion_data.ipynb`: Basic EDA of the data, including price, discount, style and article features.
* `02_feature_engineering.ipynb`: Engineering of features based on plausibility arguments.
* `03_feature_selection.ipynb`: Selection of relevant features using quantitative methods.
* `04_training.ipynb`: Training and hyper parameter optimization of a LightGBM model for 1-day forecasts.
* `05_evaluation.ipynb`: Computation and evaluation of 14-day forecasts.

## Installation
The project is based on [poetry](https://python-poetry.org/).

In order to successfully run the code follow theses steps:
1. Clone the repository.
1. Make sure [poetry](https://python-poetry.org/docs/#installation) and Python 3.10 are installed
1. Run `poetry install` from the root folder of the project.
1. Run `poetry shell` in order to start a shell within the defined environment.
1. Place the data file in the `data` folder
1. Start the notebook server as usual (e.g. `jupyter lab`)
